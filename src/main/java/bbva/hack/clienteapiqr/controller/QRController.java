package bbva.hack.clienteapiqr.controller;


import bbva.hack.clienteapiqr.model.GeneracionQR;
import bbva.hack.clienteapiqr.model.QR;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class QRController {



    @PostMapping("/qrcode")
    public ResponseEntity<byte[]> getQRCode(@RequestBody GeneracionQR generacionQR) {
        RestTemplate template = new RestTemplate();
        System.out.println(generacionQR.toString());
        byte[] resultado = template.postForObject("https://neutrinoapi.net/qr-code", generacionQR, /*QR.class*/ byte[].class);
        return new ResponseEntity<byte[]>(resultado, HttpStatus.OK);
    }


}
