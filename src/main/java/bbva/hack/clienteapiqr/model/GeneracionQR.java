package bbva.hack.clienteapiqr.model;

import org.springframework.beans.factory.annotation.Value;

public class GeneracionQR {
    private String content;
    private int width;
    private int height;
    private String fg_color;
    private String bg_color;
    private String user_id;
    private String api_key;

   /* @Value("${user.id}")
    private String user;

    @Value("${api.key}")
    private String password;*/

    public GeneracionQR(String content, int width, int height, String fg_color, String bg_color, String user_id, String api_key) {
        this.content = content;
        this.width = width;
        this.height = height;
        this.fg_color = fg_color;
        this.bg_color = bg_color;
        this.user_id = user_id;
        this.api_key = api_key;
       /* System.out.println("user_id:"+user);
        System.out.println("api_key:"+password);*/
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFg_color() {
        return fg_color;
    }

    public void setFg_color(String fg_color) {
        this.fg_color = fg_color;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    @Override
    public String toString() {
        return "GeneracionQR{" +
                "content='" + content + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", fg_color='" + fg_color + '\'' +
                ", bg_color='" + bg_color + '\'' +
                ", user_id='" + user_id + '\'' +
                ", api_key='" + api_key + '\'' +
                '}';
    }
}
