package bbva.hack.clienteapiqr.model;

public class Frase {

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Frase() {
    }

    @Override
    public String toString() {
        return "Frase{" +
                "tipo='" + tipo + '\'' +
                ", valor='" + valor + '\'' +
                '}';
    }

    private String tipo;
    private String valor;
}
